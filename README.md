# Vid2BootAnim

## About
Vid2BootAnim is a simple python script to create Android boot animations from video sources.

It can create animations from several supplied video files, each with different animation parameters (Segment type, loop count and delay).
Currently, it does not handle the 'f' animation segment type. It will be added in the future.

Vid2BootAnim uses ffmpeg to convert a video into images required for the boot animation, so any format supported by ffmpeg should work.

Vid2BootAnim is inspired by [youtube2boot](https://github.com/jaredrummler/bootanimation-scripts) by Jared Rummler 


## Requirements
- [Python](https://www.python.org) (Obviously).
- [ffmpeg](https://ffmpeg.org) (Again, obviously)
- [Imagemagick](https://www.imagemagick.org) (For the automatic detection of image size)
- [Zopfli](https://github.com/google/zopfli) (For PNG size optimization, only if optimization is enabled)


## Usage
The script is pretty self-explanatory, run <code>python vid2bootanim.py -h</code> for help on how to use it.

An example:
<code>python vid2bootanim.py --fps 60 --itype png --video video1.mp4 p 0 0 --video video2.mp4 c 10 15</code>

This creates a boot animation package that will animate with 60 FPS and convert the supplied videos to PNG-images.
The images from video1.mp4 will be created in the part0 folder, and video2.mp4 into the part1 folder.

<p>
 In desc.txt, part0 (That is, video1.mp4) will have:
 <ul>
  <li>The animation segment type 'p' (Play until boot is finished)</li>
  <li>Loop count set to 0 (Continue looping until interrupted)</li>
  <li>A delay of 0 frames (Restart animation directly after it is finished)</li>
 </ul>
 In the folder part1 (That is, video2.mp4) the entry will read:
 <ul>
  <li>Animation segment type 'c' (Play animation until completion, even if Android has finished booting)</li>
  <li>Loop count 10 (Loop animation 10 times)</li>
  <li>Delay of 15 frames (Between each animation loop wait 15 frames, which with 60fps means 1/4 second delay)</li>
 </ul>
</p>

## License
Vid2BootAnim is licensed under GNU GPL version 3. 
