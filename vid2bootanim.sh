#!/bin/bash

# TODO: Add support for multiple animations and setting parameters for each

video=$1
width=$2
height=$3
fps=$4
loopcount=$5
waitonlastimage=$6
optimize=$7

destination=$(pwd)
temp=./tmp

show_help()
{
  printf "Usage:\n"
  printf "\n"
  printf "Syntax: vid2bootanim [video_file] [width] [height] [fps] [loop_count] [wait_on_last_image] [optimize]\n"
  printf "\tWhere:\n"
  printf "\t\t[video_file] is the video you want to convert to a boot animation\n"
  printf "\t\t[width] is the width of the video in pixels\n"
  printf "\t\t[height] is the height of the video in pixels\n"
  printf "\t\t[loop_count] is the number of times to loop the animation. Set to 0 to loop until Android is loaded\n"
  printf "\t\t[wait_on_last_image] is number of frames to wait on the last image until loop restarts\n"
  printf "\t\t[optimize] enabled or disables optimizing file size with zopflipng. Enter 1 to enable, 0 to disable\n"
  printf "\n"
}

while getopts ":h" option; do
   case $option in
      h) # display Help
         show_help
         exit;;
      *)
         printf "\nOops, invalid flag\n"
         show_help
         exit;;  
   esac
done

# Make the directory for the frames to be extracted to
mkdir -p "$temp/part0"

# Use ffmpeg to extract frames from the video
ffmpeg -i "$video" "$temp/part0/00%05d.png"

if [[ "$optimize" -eq 1 ]]; then
  # Use zopfli to compress PNGs (To save memory)
  for f in "$temp/part0"/*.png ; do
      zopflipng -m "${f}" "${f}".new && mv -f "${f}".new ${f}
      # or: pngcrush -q ....
  done
fi

# Create the desc.txt file
echo "$width $height $fps" > "$temp/desc.txt"
echo "p $loopcount $waitonlastimage part0" >> "$temp/desc.txt"
echo "" >> "$temp/desc.txt"

# Create the bootanimation
pwd=$(pwd)
cd "$temp"
zip -0r bootanimation part0 desc.txt
cd "$pwd"

echo "$pwd"
cp "$temp/bootanimation.zip" "$destination"

rm -rf "$temp"
