import argparse
from datetime import datetime
import os
from pathlib import Path
import shutil
import subprocess


# Helper class to handle individual animation segment parameters
class AnimationSegmentParameters:
    FILE_NAME_POS = 0
    ANIM_SEG_TYPE_POS = 1
    ANIM_SEG_LOOP_COUNT_POS = 2
    ANIM_SEG_DELAY_POS = 3

    def __init__(self, segment_index: int, video_file: str, segment_type: str, loop_count: int, delay: int):
        self.segment_index = segment_index
        self.video_file = video_file
        self.segment_type = segment_type
        self.loop_count = loop_count
        self.delay = delay


def create_directory(current_directory: str, part_directory: str) -> str:
    segment_index = 0
    full_path = os.path.join(current_directory, 'tmp' + str(segment_index), part_directory)
    while os.path.exists(full_path):
        segment_index += 1
        full_path = os.path.join(current_directory, 'tmp' + str(segment_index), part_directory)
    os.makedirs(full_path)
    return full_path


def convert_video_to_images(input_file: str, output_directory: str, image_type: str, optimize: bool) -> bool:
    result = subprocess.run(['ffmpeg', '-i', input_file, '-qscale:v', '2', output_directory + "/00%05d." + image_type],
                            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if result.returncode == 0:
        if optimize and image_type == 'png':
            _optimize(output_directory)
        else:
            print("Image type set to jpg, not optimizing")
        return True
    else:
        print('Could not convert {} to images, aborting'.format(input_file))
        return False


def _optimize(directory: str) -> None:
    files = Path(directory).glob('*.png')
    for file in files:
        result = subprocess.run(['zopflipng', '-m', '--lossy_transparent', file.as_posix(), file.as_posix() + ".new"])
        if os.path.exists(file.as_posix() + ".new"):
            os.remove(file)
            os.rename(file.as_posix() + ".new", file.as_posix())


def create_description(directory: str, fps: int, segment_index: int, anim_type: str,
                       count: int, pause: int, image_type: str) -> None:
    base_path, part_folder = os.path.split(directory)
    description_file = open(base_path + '/desc.txt', 'a')
    if segment_index == 0:
        # Figure out image size by grabbing the first generated image file and run identify on it
        output = subprocess.run(['identify', '-format', '%w %h', directory + "/0000001." + image_type],
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        image_size = output.stdout
        description_file.write(str(image_size) + " " + str(fps))

    # TODO - Figure out how to set separate anim parameters for each part
    description_file.write("\n" + anim_type + " " + str(count) + " " + str(pause) + " " + part_folder)
    description_file.close()


def package_animation(directory: str) -> None:
    base_path, part_folder = os.path.split(directory)

    # Add empty line to end of desc.txt
    description_file = open(base_path + '/desc.txt', 'a')
    description_file.write("\n")
    description_file.close()

    os.chdir(base_path)
    # Find path folders
    part_folders = Path(base_path).glob("part*")
    folder_list = []
    # Add each part folder to a list as a string
    for folder in part_folders:
        base_path, part_folder = os.path.split(folder)
        folder_list.append(part_folder)
    # Package contents in a zip file, no compression
    subprocess.run(['zip', '-0r', 'bootanimation', 'desc.txt'] + folder_list)


def cleanup(temporary_directory: str) -> None:
    base_directory, temp_directory = os.path.split(temporary_directory)
    if os.path.exists(base_directory + '/bootanimation.zip'):
        shutil.move(base_directory + '/bootanimation.zip',
                    base_directory + '/bootanimation.zip.backup-' + datetime.now().strftime('%Y%m%d-%H%M'))
    shutil.move(temporary_directory + '/bootanimation.zip', base_directory)
    shutil.rmtree(temporary_directory)
    print('bootanimation.zip created in current directory. Bye!')


def main(anim_segment_params: list[AnimationSegmentParameters], fps: int, image_type: str,
         optimize: bool) -> None:
    current_dir = os.getcwd()
    output_directory = ""
    could_convert = False

    for segment in anim_segment_params:
        output_directory = create_directory(current_dir, "part" + str(segment.segment_index))
        could_convert = convert_video_to_images(segment.video_file, output_directory, image_type, optimize)
        if could_convert:
            create_description(output_directory, fps, segment.segment_index, segment.segment_type,
                               segment.loop_count, segment.delay, image_type)

    if output_directory != "" and could_convert:
        package_animation(output_directory)
        base_directory, part_directory = os.path.split(output_directory)
        cleanup(base_directory)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="vid2bootanim.py",
                                     description="Convert videos to an Android boot animation",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--video', nargs=4, action='append',
                        metavar=('file_name', 'segment_type', 'loop_count', 'delay'),
                        help="Video file to convert and its animation properties.\n" +
                        " segment_type: p - Loop until Android is started, c - Loop until animation is done\n" +
                        " loop_count: Times to loop animation. 0 = until boot is complete\n" +
                        " delay: Number of frames to pause after animation is done\n")
    parser.add_argument('--fps', default='30', type=int, help='Chosen FPS for animation (Default 30)')
    parser.add_argument('--itype', choices=['jpg', 'png'], default='png', type=str,
                        help='Select type of image to create from video. (Default PNG)')
    parser.add_argument('-o', '--optimize', action='store_true',
                        help='Enable/Disable PNG optimization using zopflipng')
    # parse arguments
    args = parser.parse_args()

    # Extract information about individual video files and associated parameters
    video_infos = []
    index = 0
    for video_info in args.video:
        video_infos.append(AnimationSegmentParameters(index,
                                                      video_info[AnimationSegmentParameters.FILE_NAME_POS],
                                                      video_info[AnimationSegmentParameters.ANIM_SEG_TYPE_POS],
                                                      video_info[AnimationSegmentParameters.ANIM_SEG_LOOP_COUNT_POS],
                                                      video_info[AnimationSegmentParameters.ANIM_SEG_DELAY_POS]))
        index += 1

    main(video_infos, args.fps, args.itype, args.optimize)
